<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Usuario
 * @package App\Models
 * @version May 17, 2019, 4:52 pm UTC
 *
 * @property string name
 * @property string apellidos
 * @property string direccion
 * @property string telefono
 */
class Usuario extends Model
{
    use SoftDeletes;

    public $table = 'usuarios';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'apellidos',
        'direccion',
        'telefono'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'apellidos' => 'string',
        'direccion' => 'string',
        'telefono' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'apellidos' => 'required',
        'direccion' => 'required',
        'telefono' => 'required'
    ];

    
}
